from .views import semantic
from .views import semanticSide
from .views import semanticThinSide
from django.conf import settings
from django.conf.urls import include
from django.conf.urls import url

urlpatterns = [
	url(r'semantic$', semantic.as_view(), name='semantic'),
	url(r'semanticSide$', semanticSide.as_view(), name='semanticSide'),
	url(r'semanticThinSide$', semanticThinSide.as_view(), name='semanticThinSide'),
]