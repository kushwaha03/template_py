from django.shortcuts import render
from django.conf import settings
from django.views.generic import TemplateView

# Create your views here.
class semantic(TemplateView):
    template_name = "base_semantic.html"
class semanticThinSide(TemplateView):
    template_name = "load/loadthinmenu.html"
class semanticSide(TemplateView):
    template_name = "load/loadsidemenu.html"